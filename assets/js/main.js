$(document).ready(function() {
    


    $("a.my-tool-tip").tooltip();
    $('[data-toggle="tooltip"]').tooltip();

    $('#indicatorContainer').radialIndicator({
        barColor: '#ffbe3e',
        barWidth: 15,
        initValue: 80,
        displayNumber: false,
        roundCorner : false,
        percentage: true
    });
    $('#indicatorContainer2').radialIndicator({
        barColor: '#a7c125',
        barWidth: 15,
        initValue: 20,
        displayNumber: false,
        roundCorner : false,
        percentage: true
    });

    $('.oneleng').autotab({ 
        maxlength: 1,
        tabOnSelect: true
    });


    // var _val = $(input).val();
    // $('.class').html(_val); 
    // $('.class').append(_val);
    $("#yourtlp").keyup(function(){
        $('.tlp-isi').html($(this).val());
    });
    
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z ]+$/i.test(value);
    }, "Letters and spaces only please");

    $('.formnya').validate({
        rules: {
            youremail: {
                required: true,
                email: true
            },
            yourname: {
                required: true,
                lettersonly: true,
                rangelength: [6, 20]
            },
            yourtlp: {
                required: true,
                number: true,
                rangelength: [10, 12]
            },
            exampleInputAmount: {
                required: true
            },
            exampleInputAmount2: {
                required: true
            },
            exampleInputAmount3: {
                required: true
            },
            exampleInputAmount4: {
                required: true
            },
            exampleInputAmount5: {
                required: true
            },
            exampleInputAmount6: {
                required: true
            },
            exampleInputAmount7: {
                required: true
            },
            exampleInputAmount8: {
                required: true
            },
            exampleInputAmountNo: {
                required: true
            },
            exampleInputAmount3No: {
                required: true
            },
            exampleInputAmount6No: {
                required: true
            },
            exampleInputref1: {
                required: true
            },
            exampleInputref2: {
                required: true  
            },
            InputLocation: {
                required: true
            },
            InputLocationRef: {
                required: true
            },
            yourpostcode: {
                required: true
            },
            radiovalue2: {
                required: true
            },
            radiovalue3: {
                required: true
            },
            radiovalue4: {
                required: true
            },
            radiovalue5: {
                required: true
            },
            radiovalue3no: {
                required: true
            },
            radiovalue6yes: {
                required: true
            },
            test2: {
                required: true
            },
            inputsms1: {
                required: true,
                number: true
            },
            inputsms2: {
                required: true,
                number: true
            },
            inputsms3: {
                required: true,
                number: true
            },
            inputsms4: {
                required: true,
                number: true
            }
        },
        messages: {
            youremail: {
                required: "This field is required.",
                email: "has to match the pattern: example@example.com"
            },
            yourname: {
                rangelength: "Please enter your first name and last name.",
                lettersonly: "letters only.",
                required: "This field is required."
            },
            yourtlp: {
                required: "This field is required.",
                rangelength: "Please enter a valid 10 digit mobile number."
            },
            yourpostcode: {
                required: "Please enter the correct postcode format (e.g. Campbell, ACT, 2612)."
            },
            exampleInputAmount: {
                required: "Please enter an amount."
            },
            test2: {
                required: "You must check box."
            },
            inputsms1: {
                required: "This field is required.",
                number:"Please enter numbers Only."
            },
            inputsms2: {
                required: "This field is required.",
                number:"Please enter numbers Only."
            },
            inputsms3: {
                required: "This field is required.",
                number:"Please enter numbers Only."
            },
            inputsms4: {
                required: "This field is required.",
                number:"Please enter numbers Only."
            },
            exampleInputref1: {
                required: "Please enter an amount."
            },
            exampleInputref2:{
              required: "Please enter an amount."  
            },
            exampleInputAmount: {
                required: "Please enter an amount."
            },
            exampleInputAmount2: {
                required: "Please enter an amount."
            },
            exampleInputAmount8: {
                required: "Please enter an amount."
            },
            exampleInputAmount3: {
                required: "Please enter an amount."
            },
            exampleInputAmount4: {
                required: "Please enter an amount."
            },
            exampleInputAmount5: {
                required: "Please enter an amount."
            },
            exampleInputAmount7: {
                required: "Please enter an amount."
            },
            exampleInputAmount6: {
                required: "Please enter an amount."
            },
            exampleInputAmountNo: {
                required: "Please enter an amount."
            },
            exampleInputAmount3No: {
                required: "Please enter an amount."
            },
            exampleInputAmount6No: {
                required: "Please enter an amount."
            },
            InputLocation: {
                required: "Please enter the correct postcode format (e.g. Campbell, ACT, 2612)."
            },
            InputLocationRef: {
                required: "Please enter the correct postcode format (e.g. Campbell, ACT, 2612)."
            },
            radiovalue2: {
                required: "Please select an option."
            },
            radiovalue3: {
                required: "Please select an option."
            },
            radiovalue4: {
                required: "Please select an option."
            },
            radiovalue5: {
                required: "Please select an option."
            },
            radiovalue3no: {
                required: "Please select an option."
            },
            radiovalue6yes: {
                required: "Please select an option."
            }
        }
    });


    $('.quy').autoComplete({
        minChars: 2,
        source: function(term, response){
            $.getJSON('convertcsvstates.json', { q: term }, function(data){
                term = term.toLowerCase();
                 var suggestions = [];
                 for (i=0;i<data.length;i++)
                 {
                    if (~(data[i][0]+' '+data[i][1]).toLowerCase().indexOf(term)) suggestions.push(data[i]);
                 }
                 response(suggestions);

             });
        },
        renderItem: function (item, search){
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            
            return '<div class="autocomplete-suggestion" data-langname="'+item[0]+'" data-lang="'+item[1]+'"  data-la="'+item[2]+'" data-val="'+search+'"> '+item[0].replace(re, "<b>$1</b>")+' '+item[1]+' '+item[2]+'</div>';
        },
        onSelect: function(e, term, item){
         $('.quy').val(item.data('langname')+' '+item.data('lang')+' '+item.data('la'));
        }
    });
    
    
    // validation decimal
    (function($, undefined) {

    "use strict";

    // sms
    // Numeric only control handler
    jQuery.fn.ForceNumericOnly =
    function()
    {
        return this.each(function()
        {
            $(this).keydown(function(e)
            {
                var key = e.charCode || e.keyCode || 0;
                // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
                // home, end, period, and numpad decimal
                return (
                    key == 8 || 
                    key == 9 ||
                    key == 13 ||
                    key == 46 ||
                    key == 110 ||
                    key == 190 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));
            });
        });
    };
    $("#inputsms1").ForceNumericOnly();

    // When ready.
    $(function() {
        
        var $form = $( ".formnya" );
        var $input = $form.find( ".input-form" );

        $input.on( "keyup", function( event ) {
            
            
            // When user select text in the document, also abort.
            var selection = window.getSelection().toString();
            if ( selection !== '' ) {
                return;
            }
            
            // When the arrow keys are pressed, abort.
            if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
                return;
            }
            
            
            var $this = $( this );
            
            // Get the value.
            var input = $this.val();
            
            var input = input.replace(/[\D\s\._\-]+/g, "");
                    input = input ? parseInt( input, 10 ) : 0;

                    $this.val( function() {
                        return ( input === 0 ) ? "" : input.toLocaleString( "en-US" );
                    } );
        } );
        
        /**
         * ==================================
         * When Form Submitted
         * ==================================
         */
        $form.on( "submit", function( event ) {
            
            var $this = $( this );
            var arr = $this.serializeArray();
        
            for (var i = 0; i < arr.length; i++) {
                    arr[i].value = arr[i].value.replace(/[($)\s\._\-]+/g, ''); // Sanitize the values.
            };
            
            console.log( arr );
            
            event.preventDefault();
        });
        
    });
})(jQuery);
    

    // check dulu
    var phrase = location.href;
    var word = "#";
    var type1 = phrase.substr(phrase.indexOf(word) + word.length);
    if (type1 == 'step2ref') {
        $("#step2ref").fadeIn();
    } else if (type1 == 'step2') {
        $("#step2ref").fadeOut();
        $("#step2").fadeIn();
    }
    



    // jQuery Step 
    $("#step2 .list li").click(function(){
        $("#step2").fadeOut();
        $("#step3").fadeIn();
        $("#myBar").css('width','8%');
    });

    $("#step2ref .next-step").click(function(){
        // step2
        if( $('#exampleInputref1').val().length == 0) {
             // $("#myBar").css('width','3%');
             $(".formnya").valid();
        } else {
             $("#myBar").css('width','10%');
             $("#step2ref").fadeOut();
             $("#step4-ref").fadeIn();
        }
    });

    $("#step4-ref .next-step").click(function(){
        // step2
        if( $('#InputLocationRef').val().length == 0) {
             // $("#myBar").css('width','10%');
             $(".formnya").valid();
        } else {
             $("#myBar").css('width','15%');
             $("#step4-ref").fadeOut();
             $("#step5-ref").fadeIn();
        }
    });

    $("#step5-ref .next-step").click(function(){
        // step2
        if( $('#exampleInputref2').val().length == 0) {
             // $("#myBar").css('width','15%');
             $(".formnya").valid();
        } else {
             $("#myBar").css('width','20%');
             $("#step5-ref").fadeOut();
             $("#step7").fadeIn();
        }
    });

    $("#step3 .next-step").click(function(){
        // step2
        if( $('#exampleInputAmount').val().length == 0) {
             // $("#myBar").css('width','8%');
             $(".formnya").valid();
        } else {
             $("#myBar").css('width','15%');
             $("#step3").fadeOut();
             $("#step4").fadeIn();
        }
    });

    $("#step4 .next-step").click(function(){
        // step3
        if( $('#InputLocation').val().length == 0) {
             // $("#myBar").css('width','15%');
             $(".formnya").valid();
        } else {
             $("#myBar").css('width','20%');
             $("#step4").fadeOut();
             $("#step5").fadeIn();
        }    
    });

    $("#step5 .next-step").click(function(){
        // step4
        if( $('#exampleInputAmount2').val().length == 0) {
             // $("#myBar").css('width','20%');
             $(".formnya").valid();
        } else {
             $("#myBar").css('width','25%');
             $("#step5").fadeOut();
             $("#step6").fadeIn();
        }
    });

    // cek radio button value
    $('.radio-group .radio').click(function(){
        $(this).parent().find('.radio').removeClass('selected');
        $(this).addClass('selected');
        var val = $(this).attr('data-value');
        //alert(val);
        $(this).parent().find('input').val(val);
    });



    $("#step6 .next-step").click(function(){
         if($('#radiovalue').val().length == 0) {
             // $("#myBar").css('width','25%');
             $(".error").text('Please select an option');
        } else {
             $("#myBar").css('width','30%');
             $("#step6").fadeOut();
             $("#step7").fadeIn();
        }
    });
    
    $("#step7 .next-step").click(function(){
        if($('#radiovalue2').val().length == 0) {
             // $("#myBar").css('width','30%');
             $(".error").text('Please select an option');
        } 

        if($('#radiovalue2').val() == 'yes') {
             $("#myBar").css('width','35%');
             $("#step7").fadeOut();
             $("#step8-yes").fadeIn();
        } else if ($('#radiovalue2').val() == 'no')  {
            $("#myBar").css('width','35%');
             $("#step7").fadeOut();
             $("#step8-no").fadeIn();
        } 
    });

    $("#step8-yes .next-step").click(function(){
        // step6
        console.log($('#radiovalue3').val());
        if($('#radiovalue3').val().length == 0) {
             // $("#myBar").css('width','35%');
             $(".formnya").valid();
        } else {
            $("#myBar").css('width','40%');
            $("#step8-yes").fadeOut();
            $("#step9-yes").fadeIn();
        }
    });

    $("#step8-no .next-step").click(function(){
        // step6
        console.log($('#radiovalue3no').val());
        if($('#radiovalue3no').val().length == 0) {
             // $("#myBar").css('width','35%');
             $(".formnya").valid();
        } else {
            $("#myBar").css('width','40%');
            $("#step8-no").fadeOut();
            $("#step9-no").fadeIn();
        }
    });

    $("#step9-yes .next-step").click(function(){
        // step6
        console.log($('#radiovalue4').val());
        if($('#radiovalue4').val().length == 0) {
             // $("#myBar").css('width','40%');
             $(".formnya").valid();
        } else {
            $("#myBar").css('width','45%');
            $("#step9-yes").fadeOut();
            $("#step10-yes").fadeIn();
        }
    });

    $("#step9-no .next-step").click(function(){
        // step6
        if($('#exampleInputAmountNo').val().length == 0) {
             // $("#myBar").css('width','40%');
             $(".formnya").valid();
        } else {
            $("#myBar").css('width','45%');
            $("#step9-no").fadeOut();
            $("#step10-no").fadeIn();
        }
    });

    $("#step10-yes .next-step").click(function(){
        // step6
        if( $('#exampleInputAmount3').val().length == 0) {
             // $("#myBar").css('width','45%');
             $(".formnya").valid();
        } else {
             $("#myBar").css('width','50%');
             $("#step10-yes").fadeOut();
             $("#step11-yes").fadeIn();
        }   
    });

    $("#step10-no .next-step").click(function(){
        // step6
        if( $('#exampleInputAmount3No').val().length == 0) {
             // $("#myBar").css('width','45%');
             $(".formnya").valid();
        } else {
             $("#myBar").css('width','50%');
             $("#step10-no").fadeOut();
             $("#step12").fadeIn();
        }   
    });

    $("#step11-yes .next-step").click(function(){
        // step6
        if( $('#exampleInputAmount4').val().length == 0) {
             // $("#myBar").css('width','50%');
             $(".formnya").valid();
        } else {
             $("#myBar").css('width','55%');
             $("#step11-yes").fadeOut();
             $("#step12").fadeIn();
        }
    });

    $("#step12 .next-step").click(function(){
        // step6
        if($('#radiovalue5').val().length == 0) {
             // $("#myBar").css('width','55%');
             $(".error").text('Please select an option');
        } 

        if($('#radiovalue5').val() == 'yes') {
             $("#myBar").css('width','60%');
             $("#step12").fadeOut();
             $("#step13-yes").fadeIn();
        } else if ($('#radiovalue5').val() == 'no')  {
            $("#myBar").css('width','60%');
             $("#step12").fadeOut();
             $("#step13-no").fadeIn();
        } 
    });

    $("#step13-yes .next-step").click(function(){
        // step6
        if( $('#exampleInputAmount5').val().length == 0) {
             // $("#myBar").css('width','60%');
             $(".formnya").valid();
        } else {
             $("#myBar").css('width','65%');
             $("#step13-yes").fadeOut();
             $("#step14-yes").fadeIn();
        }
    });

    $("#step13-no .next-step").click(function(){
        // step6
        if( $('#exampleInputAmount6No').val().length == 0) {
             // $("#myBar").css('width','60%');
             $(".formnya").valid();
        } else {
             $("#myBar").css('width','65%');
             $("#step13-no").fadeOut();
             $("#get-my-deal").fadeIn();
        }
    });

    $("#step14-yes .next-step").click(function(){
        // step6
        if( $('#exampleInputAmount6').val().length == 0) {
             // $("#myBar").css('width','65%');
             $(".formnya").valid();
        } else {
             $("#myBar").css('width','70%');
             $("#step14-yes").fadeOut();
             $("#step15-yes").fadeIn();
        }
    });

    $("#step15-yes .next-step").click(function(){
        // step6
        if($('#radiovalue6yes').val().length == 0) {
             // $("#myBar").css('width','70%');
             $(".error").text('Please select an option');
        } 

        if($('#radiovalue6yes').val() == 'yes') {
             $("#myBar").css('width','75%');
             $("#step15-yes").fadeOut();
             $("#step16-yes").fadeIn();
        } else if ($('#radiovalue6yes').val() == 'no')  {
            $("#myBar").css('width','75%');
             $("#step15-yes").fadeOut();
             $("#step17").fadeIn();
        } 
    });

    $("#step16-yes .next-step").click(function(){
        // step6
        if( $('#exampleInputAmount7').val().length == 0) {
             // $("#myBar").css('width','75%');
             $(".formnya").valid();
        } else {
             $("#myBar").css('width','80%');
             $("#step16-yes").fadeOut();
             $("#step17").fadeIn();
        }
    });

    $("#step17 .next-step").click(function(){
        // step6
        if( $('#exampleInputAmount8').val().length == 0) {
             // $("#myBar").css('width','80%');
             $(".formnya").valid();
        } else {
             $("#myBar").css('width','85%');
             $("#step17").fadeOut();
             $("#get-my-deal").fadeIn();
        }
    });

    $("#get-my-deal .next-step").click(function(){
        if( $('#yourname').val() == '' && ($('#yourtlp').val() == '' &&  $('#youremail').val() == '' && $('#yourpostcode').val() == '' && !$("#test2").is(':checked') )) {
            // $("#myBar").css('width','85%'); 
            $(".formnya").valid();
        } else {
            $("#get-my-deal").fadeOut();
            $("#sms").fadeIn();
            $("#myBar").css('width','90%');
        }
    });

    $("#sms .next-step").click(function(){
        if( $('#inputsms1').val() != '' && $('#inputsms2').val() != '' && $('#inputsms3').val() != '' && $('#inputsms4').val() != '') {
            $("#myBar").css('width','100%');
            $("#sms").fadeOut();
            $("#result").fadeIn();
            $(".tagline-white-wrapper").fadeOut();
            $(".tagline-green-wrapper").fadeIn();
            $("main").css('background', '#f8f8f8');
            $("main").css('border-bottom', '1px solid #ebebeb')
        } else {
            $("#myBar").css('width','90%');
            $(".formnya").valid();
        }
    });


    $('.autoplay').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      arrows: false
    });



});